package es.ull.pai.Practica4.ejercicio5;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by eleazardd on 6/03/16.
 */
public class Ejercicio5 extends JFrame {
    Ejercicio5() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 20));
        add(new JLabel("hola"));
    }

    public static void main(String[] args) {
        Ejercicio5 frame = new Ejercicio5();


        frame.setTitle("Ejercicio 1");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        boolean errors;
        do {
            errors = false;
            try {
                Scanner reader = new Scanner(System.in);  // Reading from System.in
                System.out.println("Enter a first number: ");
                String n = reader.nextLine();

                System.out.println(hex2Decimal(n));
            }
            catch (HexFormatException err) {
                System.out.println("No ha introducido numeros correctamente" + err.getMessage());
                errors = true;
            }
            catch (Exception err) {
                System.out.println("Error");
                errors = true;
            }
        } while (!errors);
    }

    public static Integer hex2Decimal(String hexDecimal) throws HexFormatException { // 0x0ffff0ff4f5ec
        Pattern p = Pattern.compile("0x([0-9abcdef]+)",Pattern.CASE_INSENSITIVE);
        Matcher match = p.matcher(hexDecimal);

        Integer result = 0;
        if (match.find()) {
            int exp = match.group(1).length() - 1;
            System.out.println(match.group(1));
            for (int value : match.group(1).toLowerCase().chars().map(i -> (i >= 97) ? i - 87 : i - 48).toArray()) {
                System.out.println(value);
                result += value * (int)Math.pow(16, exp);
                exp--;
            }
        }
        else {
            throw new HexFormatException("Bad input: " + hexDecimal);
        }
        return result;
    }
}
