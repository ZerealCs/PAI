package es.ull.pai.Practica4.ejercicio2;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This program wait a input by user to predict a number hidden by program, if user
 * is near or equal to random number is gains several awards
 */
public class Ejercicio2 extends JFrame {

    public Ejercicio2() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 20));
        add(new JLabel("hola"));
    }

    public static void main(String[] args) {
        Ejercicio2 frame = new Ejercicio2();

        frame.setTitle("Ejercicio 1");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        final String USAGE = new String("USAGE: ./practica3 file  # where must be a file");
        ParseCommands commands = new ParseCommands(args, USAGE);
        try {
            String content = new String(Files.readAllBytes(Paths.get(commands.getString())));

            Syntax html = new Syntax(content);
            Files.write(Paths.get(commands.getString()), html.toString().getBytes());

        }
        catch (FileNotFoundException err) {
            System.out.println("No se encontro el archivo");
        }
        catch (IOException err) {
            System.out.println("Error al cargar el archivo");
        }
    }
}