package es.ull.pai.Practica4.ejercicio4;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by eleazardd on 6/03/16.
 */
public class Ejercicio4 extends JFrame{
    Ejercicio4() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 20));
        add(new JLabel("hola"));
    }

    public static void main(String[] args) {
        Ejercicio4 frame = new Ejercicio4();

        frame.setTitle("Ejercicio 1");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        sumNumbers();
    }

    public static void sumNumbers() {
        boolean errors;
        do {
            errors = false;

            try {
                Scanner reader = new Scanner(System.in);  // Reading from System.in
                System.out.println("Enter a first number: ");
                int n = reader.nextInt();
                System.out.println("Enter a second number: ");
                int n2 = reader.nextInt();
                System.out.println(n + n2);
            }
            catch (NumberFormatException err) {
                System.out.println("No ha introducido numeros correctamente");
                errors = true;
            }
            catch (Exception err) {
                System.out.println("Error");
                errors = true;
            }
        } while (errors);
    }
}
